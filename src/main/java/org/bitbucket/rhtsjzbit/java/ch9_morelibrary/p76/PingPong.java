package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p76;

public class PingPong {

    public static void main(String[] args) {
        Thread t = new Thread() {
            @Override
            public void run() {
                pong();
            }
        };
        t.start();
        t.run();
        System.out.println("Ping");
    }

    static synchronized void pong() {
        System.out.println("Pong");
    }
}
