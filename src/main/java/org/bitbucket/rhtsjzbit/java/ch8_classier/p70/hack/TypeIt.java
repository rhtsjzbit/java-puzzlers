package org.bitbucket.rhtsjzbit.java.ch8_classier.p70.hack;

import org.bitbucket.rhtsjzbit.java.ch8_classier.p70.click.CodeTalk;

public class TypeIt {

    private static class ClickIt extends CodeTalk {
        void printMessage() {
            System.out.println("Hack");
        }
    }

    public static void main(String[] args) {
        /**
         * In summary, package-private methods cannot be directly overridden outside
         the package in which they’re declared
         */
        ClickIt clickIt = new ClickIt();
        clickIt.doIt();
    }
}
