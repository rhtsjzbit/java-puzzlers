package org.bitbucket.rhtsjzbit.java.ch3_character.p20;

import java.util.regex.Pattern;

public class Me {
    public static void main(String[] args) {
//        System.out.println(Me.class.getName().replaceAll("\\.", "/") + ".class");
        String regex = Pattern.quote(".");
        System.out.println(regex);
        System.out.println(Me.class.getName().replaceAll(regex, "/") + ".class");
    }
}
