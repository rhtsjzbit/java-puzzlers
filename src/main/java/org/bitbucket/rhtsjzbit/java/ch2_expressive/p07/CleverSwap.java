package org.bitbucket.rhtsjzbit.java.ch2_expressive.p07;


/**
 * The lesson is simple: Do not assign to the same variable more than once in
 * a single expression.
 */
public class CleverSwap {
    public static void main(String[] args) {
        int x = 1984;  // 0x7c0
        int y = 2001;  // 0x7d1
//        x ^= y ^= x ^= y;
        // Rube Goldberg would approve, but don’t ever do this!
        y = (x ^= (y ^= x)) ^ y;
        System.out.println("x = " + x + "; y = " + y);
    }
}
