package org.bitbucket.rhtsjzbit.java.ch6_classy.p53;

public class MyThing extends Thing {

    /**
     * This solution uses an alternate constructor invocation [JLS 8.8.7.1].
     */

    private final int arg;

    public MyThing() {
        this(SomeOtherClass.func());
    }

    private MyThing(int i) {
        super(i);
        arg = i;
    }
}
