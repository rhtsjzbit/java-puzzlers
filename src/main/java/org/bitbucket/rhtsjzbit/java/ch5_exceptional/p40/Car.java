package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p40;

public class Car {
    // 实例初始化操作是线与构造器的程序体而运行的.实例化操作抛出的任何异常都会传播给构造器
    private static Class enginClass = String.class;
    private String engine = (String) enginClass.newInstance();


    public Car() throws IllegalAccessException, InstantiationException {
    }
}
