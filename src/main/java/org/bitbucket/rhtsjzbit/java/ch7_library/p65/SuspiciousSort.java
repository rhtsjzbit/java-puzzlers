package org.bitbucket.rhtsjzbit.java.ch7_library.p65;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class SuspiciousSort {
    public static void main(String[] args) {
        Random rnd = new Random();
        Integer[] arr = new Integer[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt();
        }

        Comparator<Integer> cmp = (o1, o2) -> o2 - o1;
//        Arrays.sort(arr, cmp);
        Arrays.sort(arr, Collections.reverseOrder());
        for (int i=0; i< 10; i++){
            System.out.println("arr = " + arr[i]);
        }
        System.out.println(order(arr));
        "a".getBytes(Charset.forName("UTF-8"));
        "a".length();
    }

    enum Order {ASCENDING, DESCENDING, CONSTANT, UNORDERED}

    static Order order(Integer[] a) {
        boolean ascending = false;
        boolean descending = false;
        for (int i = 1; i < a.length; i++) {
            ascending |= (a[i] > a[i - 1]);
            descending |= (a[i] < a[i - 1]);
        }
        if (ascending && !descending) {
            return Order.ASCENDING;
        }
        if (descending && !ascending) {
            return Order.DESCENDING;
        }
        if (!ascending) {
            return Order.CONSTANT;
        }
        return Order.UNORDERED;

    }
}
