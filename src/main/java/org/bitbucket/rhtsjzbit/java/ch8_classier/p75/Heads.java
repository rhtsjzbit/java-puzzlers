package org.bitbucket.rhtsjzbit.java.ch8_classier.p75;

class Heads extends CoinSide {
    private Heads() {
    }

    public static final Heads INSTANCE = new Heads();

    @Override
    public String toString() {
        return "heads";
    }
}
