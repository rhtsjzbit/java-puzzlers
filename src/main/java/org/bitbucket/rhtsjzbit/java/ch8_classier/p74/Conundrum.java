package org.bitbucket.rhtsjzbit.java.ch8_classier.p74;


/**
 * If you do
 * overload a method, make sure that all overloadings behave identically.
 */
public class Conundrum {

    public static void main(String[] args) {
        Enigma e = new Enigma();
        System.out.println(e.equals(e));
    }
}
