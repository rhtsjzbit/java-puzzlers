package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p83;

public class Dog extends Exception {
    public static final Dog INSTANCE = new Dog();

    private Dog() {
    }

    @Override
    public String toString() {
        return "Woof";
    }

    private Object readResolve() {
        return INSTANCE;
    }
}
