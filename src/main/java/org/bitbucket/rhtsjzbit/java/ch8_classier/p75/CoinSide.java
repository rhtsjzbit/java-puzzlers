package org.bitbucket.rhtsjzbit.java.ch8_classier.p75;

import java.util.Random;

public class CoinSide {

    private static Random rnd = new Random();

    public static CoinSide flip() {
        return rnd.nextBoolean() ? Heads.INSTANCE : Tails.INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(flip());
        }
    }
}

