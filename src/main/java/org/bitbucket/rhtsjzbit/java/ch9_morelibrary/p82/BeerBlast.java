package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p82;

import java.io.IOException;
import java.io.InputStream;

public class BeerBlast {
    private static int MAX = 10;

    static final String COMMAND = "java -cp src/main/java org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p82.BeerBlast ";

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 1 && args[0].equals("slave")) {
            for (int i = 99; i > 0; i--) {
                System.out.println(i + " bottles of beer on the wall");
                System.out.println(i + " bottles of beer");
                System.out.println("You take one down, pass it around,");
                System.out.println((i - 1) + " bottles of beer on the wall");
                System.out.println();
            }

        } else {
            // Master
            while (MAX ++ < 10){
                Process process = Runtime.getRuntime().exec(COMMAND);
                drainInBackgroud(process.getInputStream());
                int exitValue = process.waitFor();
                System.out.println("exit value = " + exitValue);
            }
        }
    }

    static void drainInBackgroud(final InputStream is) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (is.read() >= 0) ;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
