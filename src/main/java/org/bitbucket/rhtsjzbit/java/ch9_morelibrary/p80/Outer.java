package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p80;

import java.lang.reflect.InvocationTargetException;

public class Outer {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        new Outer().greetWorld();
    }

    private void greetWorld() throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        System.out.println(Inner.class.newInstance());


        // 发现这种方式不起作用尽管书上说这样可以，目前来看只有静态类的这种方式可以
//        Constructor c = Inner.class.getConstructor(Outer.class);
//        System.out.println(c.newInstance(Outer.class));
    }


//    public class Inner {
//        @Override
//        public String toString() {
//            return "Hello World";
//        }
//    }

    public static class Inner {
        @Override
        public String toString() {
            return "Hello World";
        }
    }
}
