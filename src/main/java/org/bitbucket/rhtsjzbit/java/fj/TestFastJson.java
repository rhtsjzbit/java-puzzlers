package org.bitbucket.rhtsjzbit.java.fj;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class TestFastJson {
    public static void main(String[] args) {
        User user1 = new User();
        user1.setUsername("xiaoming");
        user1.setSex("male");
        System.out.println("obj name:" + user1.getClass().getName());

        String serializedStr1 = JSON.toJSONString(user1, SerializerFeature.WriteClassName);
        System.out.println("serializedStr1=" + serializedStr1);

        User user2 = JSON.parseObject(serializedStr1, User.class);
        System.out.println(user2);
        System.out.println("obj name:" + user2.getClass().getName() + "\n");
    }
}
