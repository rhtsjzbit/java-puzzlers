package org.bitbucket.rhtsjzbit.java.ch6_classy.p55;

public class Creator {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Creature();
        }
        System.out.println(Creature.numCreated());
    }
}
