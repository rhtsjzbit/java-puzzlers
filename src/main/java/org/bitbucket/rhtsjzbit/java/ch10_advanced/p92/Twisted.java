package org.bitbucket.rhtsjzbit.java.ch10_advanced.p92;

public class Twisted {

    /**
     * private members are never inherited [JLS 8.2].
     */

    private final String name;

    public Twisted(String name) {
        this.name = name;
    }

    private String name() {
        return name;
    }

    private void reproduce() {
        new Twisted("reproduce") {
            void printName() {
                System.out.println(name());
            }
        }.printName();
    }

    public static void main(String[] args) {
        new Twisted("main").reproduce();
    }
}
