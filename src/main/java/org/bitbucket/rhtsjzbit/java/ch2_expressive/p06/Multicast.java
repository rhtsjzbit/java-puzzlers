package org.bitbucket.rhtsjzbit.java.ch2_expressive.p06;

public class Multicast {

    public static void main(String[] args) {
        System.out.println(-1);
        System.out.println((byte) -1);
        System.out.println((char) (byte) -1);
        System.out.println((int) (char) (byte) -1);
    }
}
