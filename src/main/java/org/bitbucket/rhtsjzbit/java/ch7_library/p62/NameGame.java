package org.bitbucket.rhtsjzbit.java.ch7_library.p62;

import java.util.IdentityHashMap;
import java.util.Map;

public class NameGame {

    public static void main(String[] args) {
        /**
         * this class implements the Map interface with a hash
         table, using reference-equality in place of [value]-equality when comparing keys
         */
        Map<String, String> m = new IdentityHashMap<>();
        m.put("Mickey", "Mouse");
        m.put("Mickey", "Mantle");
        System.out.println(m.size());
    }
}
