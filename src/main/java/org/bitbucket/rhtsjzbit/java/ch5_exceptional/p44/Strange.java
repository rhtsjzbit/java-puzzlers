package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p44;

public class Strange {

    public static void main(String[] args) throws Exception {
        try {
            Object m = Class.forName("org.bitbucket.rhtsjzbit.java.ch5_exceptional.p44.Missing").newInstance();
        } catch (ClassNotFoundException e) {
            System.err.println("Got it!");
        }
    }
}
