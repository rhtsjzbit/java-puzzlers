package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p42;

public class Loop {

    public static void main(String[] args) {
        int[][] tests = {{6, 5, 4, 3, 2, 1}, {1, 2}, {1, 2, 3}, {1, 2, 3, 4}, {1}};
        int successCount = 0;

        // 不要这样来实现循环
//        try {
//            int i = 0;
//            while (true) {
//                if (thirdElementIsThree(tests[i++])) {
//                    successCount++;
//                }
//            }
//        } catch (ArrayIndexOutOfBoundsException e) {
//            e.printStackTrace();
//            // No more tests to process
//        }

        for (int i = 0; i < tests.length; i++) {
            if (thirdElementIsThree(tests[i])) {
                successCount++;
            }
        }
        System.out.println(successCount);
    }

    private static boolean thirdElementIsThree(int[] a) {
        // 在两个bool值之间 使用 & vs && 与 | vs ||
        return a.length >= 3 && a[2] == 3;
    }
}
