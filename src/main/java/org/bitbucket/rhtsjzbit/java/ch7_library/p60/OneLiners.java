package org.bitbucket.rhtsjzbit.java.ch7_library.p60;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

public class OneLiners {

    public static void main(String[] args) {
        String[] a = new String[]{"spam", "sausage", "spam", "spam", "bacon", "spam", "tomato", "spam"};
        System.out.println(Arrays.deepToString(a));
        List l = withoutDuplicates(Arrays.asList(a));
        for (Object o : l) {
//            System.out.println(o);
        }

        String s = "fear, surprise,             ruthless efficiency, an almost fanatical devotion to the Pope, nice red uniforms";
        String[] ss = parse(s);
        for (String token : ss) {
//            System.out.println(token);
        }

        System.out.println(hasMoreBitsSet(1, 7));
    }

    static <E> List<E> withoutDuplicates(List<E> original) {
        return new ArrayList<>(new LinkedHashSet<>(original));
    }

    static String[] parse(String s) {
        return s.split(",\\s*");
    }

    static boolean hasMoreBitsSet(int i, int j) {
        System.out.println(Integer.toBinaryString(i));
        System.out.println(Integer.toBinaryString(j));
        System.out.println(Integer.bitCount(i));
        System.out.println(Integer.bitCount(j));
        return (Integer.bitCount(i) > Integer.bitCount(j));
    }
}
