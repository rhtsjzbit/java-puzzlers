package org.bitbucket.rhtsjzbit.java.ch2_expressive.p05;

public class JoyOfHex {
    public static void main(String[] args) {
        System.out.println(4294967296L + 0xcafebabe);
        System.out.println(1000 * 1000 * 60 * 60 * 24);
        System.out.println(1000 * 60 * 60 * 24);
    }
}
