package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p43;

import java.io.FileNotFoundException;

public class TigerThrower<T extends Throwable> {

    public static void sneakyThow(Throwable t) {
        new TigerThrower<Error>().sneakyThrow2(t);
    }

    private void sneakyThrow2(Throwable t) throws T {
        throw (T) t;
    }

    public static void main(String[] args) {
        sneakyThow(new FileNotFoundException());
    }
}
