package org.bitbucket.rhtsjzbit.java.ch2_expressive.p08;

/**
 * In summary, it is generally best to use the same type for the second and
 * third operands in conditional expressions
 */
public class DosEquis {
    public static void main(String[] args) {
        char x = 'X';
        int i = 0;
        System.out.println(true ? x : 0);
        System.out.println(true ? x : i);
        System.out.println(false ? i : x);
        System.out.println(false ? 0 : x);
    }
}
