package org.bitbucket.rhtsjzbit.java.ch10_advanced.p87;

public class Transitive {
    public static void main(String[] args) {
        System.out.println(Double.NaN == Double.NaN);

        long x = Long.MAX_VALUE;
        double y = (double) Long.MAX_VALUE;
        long z = Long.MAX_VALUE - 1;
        double y1 = (double) z;

        System.out.println(x == y);
        System.out.println(y == z);
        System.out.println(x == z);

        System.out.println(y == y1);
        System.out.println(y1 == z);

        System.out.println(x);
        System.out.println((long) y);
        System.out.println((long) y1);
        System.out.println(z);

        System.out.println(y);
        System.out.println(y1);
    }
}
