package org.bitbucket.rhtsjzbit.java.ch10_advanced.p91;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerialKiller {

    /**
     * If a HashSet , Hashtable , or HashMap will be serialized, ensure that its contents
     * do not refer back to it, directly or indirectly. By contents, we mean elements,
     * keys, and values.
     *
     * @param args
     */

    public static void main(String[] args) {
        Sub sub = new Sub(666);
        sub.checkInvariant();

        Sub copy = (Sub) deepCopy(sub);
        copy.checkInvariant();
    }


    // Copies its argument via serialization (See Puzzle 83)
    public static Object deepCopy(Object obj) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            new ObjectOutputStream(bos).writeObject(obj);

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            return new ObjectInputStream(bis).readObject();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

}
