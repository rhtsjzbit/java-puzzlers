package org.bitbucket.rhtsjzbit.java.ch6_classy.p51;

public class ColorPoint extends Point {

    /**
     * Circular class initialization is a necessary evil, but cir-
     * cular instance initialization can and should always be avoided.
     */

    private final String color;


    public ColorPoint(int x, int y, String color) {
        super(x, y);
        this.color = color;
    }

    @Override
    protected String makeName() {
        return super.makeName() + ":" + color;
    }

    public static void main(String[] args) {
        System.out.println(new ColorPoint(4, 2, "purple"));
    }
}
