package org.bitbucket.rhtsjzbit.java.ch6_classy.p54;

public class Null {

    /**
     * Qualify static method invocations with a type, or don’t qualify them at all.
     */

    public static void greet() {
        System.out.println("Hello world!");
    }

    public static void main(String[] args) {
        ((Null) null).greet();
    }
}
