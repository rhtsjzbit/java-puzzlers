package org.bitbucket.rhtsjzbit.java.ch10_advanced.p94;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Shuffle {
    private static Random rnd = new Random();

    public static void shuffle(Object[] a) {
        for (int i = 0; i < a.length; i++) {
//            swap(a, i, rnd.nextInt(a.length));
//            swap(a, i, i + rnd.nextInt(a.length - i));
            Collections.shuffle(Arrays.asList(a));
        }
    }

    private static void swap(Object[] a, int i, int j) {
        Object tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static void main(String[] args) {
        Object[] a = new Object[]{"a", "b", "c", "d"};
        for (int j = 0; j < 10000; j++) {
            shuffle(a);
            for (int i = 0; i < a.length; i++) {
                System.out.print(a[i] + (i == a.length - 1 ? "" : "_"));
            }
            System.out.println();
        }

    }
}
