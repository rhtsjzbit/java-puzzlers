package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p43;

public class Thrower {
    private static Throwable t;

    private Thrower() throws Throwable {
        throw t;
    }

    public static synchronized void sneakyThrow(Throwable t) {
        Thrower.t = t;
        try {
            Thrower.class.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } finally {
            Thrower.t = null; //Avoid memory leak
        }
    }

    public static void main(String[] args) {
        sneakyThrow(new Exception());
    }
}
