package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p85;

/**
 * In summary, waiting for a background thread during class initialization is
 * likely to result in deadlock.
 */

public class Lazy {
    private static boolean initialized = false;

    static {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                initialized = true;
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new AssertionError(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(initialized);
    }
}
