package org.bitbucket.rhtsjzbit.java.ch6_classy.p47;

public class Counter {

    private static int count;

    public static void increment() {
        count++;
    }

    public static int getCount() {
        return count;
    }
}
