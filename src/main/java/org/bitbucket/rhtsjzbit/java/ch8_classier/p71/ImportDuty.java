package org.bitbucket.rhtsjzbit.java.ch8_classier.p71;

//import static java.util.Arrays.toString;

import java.util.Arrays;

public class ImportDuty {

    public static void main(String[] args) {
        printArgs(1, 2, 3, 4, 5);
    }


    static void printArgs(Object... args) {
//        System.out.println(toString(args));
        System.out.println(Arrays.toString(args));
    }

}
