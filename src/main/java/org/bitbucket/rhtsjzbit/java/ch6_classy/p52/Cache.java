package org.bitbucket.rhtsjzbit.java.ch6_classy.p52;

public class Cache {

    /**
     * In summary, think about class initialization order, especially when it is
     nontrivial.
     */

    static {
        initializeIfNecessary();
    }

    private static int sum;
    private static int sum1 = computeSum();
    private static boolean initialized = false;

    private static synchronized void initializeIfNecessary() {
        if (!initialized) {
            for (int i = 0; i < 100; i++) {
                sum += i;
            }
            initialized = true;
        }
    }

    private static int computeSum() {
        int result = 0;
        for (int i = 0; i < 100; i++) {
            result += i;
        }
        return result;
    }

    public static int getSum() {
        initializeIfNecessary();
        return sum;
    }

    public static int getSum1() {
        return sum1;
    }
}
