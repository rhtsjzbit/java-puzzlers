package org.bitbucket.rhtsjzbit.java.ch8_classier.p72;

/**
 * the final modifier means something completely different
 * on methods and fields. On a method, final means that the method may not be
 * overridden (for instance methods) or hidden (for static methods) [JLS 8.4.3.3]. On
 * a field, final means the field may not be assigned more than once [JLS 8.3.1.2].
 */

public class DoubleJeopardy extends Jeopardy {
    public static final String PRIZE = "2 cents";

    public static void main(String[] args) {
        System.out.println(DoubleJeopardy.PRIZE);
    }
}
