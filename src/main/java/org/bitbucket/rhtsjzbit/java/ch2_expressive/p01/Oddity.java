package org.bitbucket.rhtsjzbit.java.ch2_expressive.p01;

import java.util.Random;

public class Oddity {

    public static boolean isOdd(int i) {
//        return i % 2 == 1;
//        return i % 2 != 0;
        return (i & 1) != 0;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(isOdd(new Random().nextInt()));
        }
    }
}
