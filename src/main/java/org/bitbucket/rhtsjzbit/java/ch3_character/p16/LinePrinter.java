package org.bitbucket.rhtsjzbit.java.ch3_character.p16;

/**
 * Avoid Unicode escapes except
 * where they are truly necessary. They are rarely necessary.
 */


public class LinePrinter {
    public static void main(String[] args) {
        /*
         // Note: \u000A is Unicode representation of linefeed (LF)
        */
//        char c = 0x00A;
        char c = '\n';
        System.out.println(1);
//        System.out.println(c);

//        System.out.println();
//        System.out.println();

        System.out.printf("%n%n");
        System.out.println(1);
    }
}
