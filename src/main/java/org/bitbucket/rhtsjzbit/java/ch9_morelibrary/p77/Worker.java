package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p77;

import java.util.Timer;
import java.util.TimerTask;

public class Worker extends Thread {
    private volatile boolean quitingTime = false;

    private final Object lock = new Object();

    @Override
    public void run() {
        while (!quitingTime) {
            pretendToWork();
        }
        System.out.println("Beer is good");
    }

    private void pretendToWork() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {

        }
    }

    void quit() throws InterruptedException {
        synchronized (lock) {
            quitingTime = true;
            join();
        }
    }



    void keepWorking() {
        synchronized (lock) {
            quitingTime = false;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final Worker worker = new Worker();
        worker.start();

        Timer t = new Timer(true);
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                worker.keepWorking();
            }
        }, 500);

        Thread.sleep(400);
        worker.quit();

    }
}
