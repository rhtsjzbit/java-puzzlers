package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p39;

public class UnwelcomeGuest {
    public static final long GUEST_USER_ID = -1;

    public static final long USER_ID = getUserIdOrGuest();

    public static void main(String[] args) {
        System.out.println("User ID: " + USER_ID);
    }

    private static long getUserIdOrGuest() {
        try {
            return getUserIdFromEnvironment();
        } catch (IdUnavailableException e) {
            System.out.println("Logging in as guest");
            return GUEST_USER_ID;
        }
    }

    private static long getUserIdFromEnvironment() throws IdUnavailableException {
        throw new IdUnavailableException(); // Simulate an error

    }
}

class IdUnavailableException extends Exception {

}
