package org.bitbucket.rhtsjzbit.java.ch3_character.p12;

import java.util.Arrays;

public class Abc {
    public static void main(String[] args) {
        String letters = "ABC";
        char[] numbers = {'1', '2', '3'};
        Object[] obj = {'1', '2', '3'};
        System.out.println(letters + " easy as " + numbers);
        System.out.println(letters + " easy as " + Arrays.toString(numbers));
        System.out.println(numbers);
        System.out.println(String.valueOf(numbers));
        System.out.println(obj);
        System.out.println(String.valueOf(obj));
    }
}
