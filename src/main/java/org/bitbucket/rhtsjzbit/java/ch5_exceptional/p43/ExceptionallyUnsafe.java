package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p43;

public class ExceptionallyUnsafe {

    // Don't do this - circumvents exception checking!
    public static void sneakyThrow(Throwable t) {
        Thread.currentThread().stop(t); // Deprecated!!
    }

    public static void main(String[] args) {
        sneakyThrow(new Exception());
    }
}
