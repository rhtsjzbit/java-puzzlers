package org.bitbucket.rhtsjzbit.java.ch8_classier.p66;

public class Derived extends Base {
    private String className = "Derived";

    @Override
    public String getClassName() {
        return "Derived";
    }
}
