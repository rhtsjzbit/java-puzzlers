package org.bitbucket.rhtsjzbit.java.ch8_classier.p75;

import java.util.Random;

public class CoinSide1 {

    public static final CoinSide1 HEADS = new CoinSide1("heads");
    public static final CoinSide1 TAILS = new CoinSide1("tails");

    private final String name;

    public CoinSide1(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    private static Random rnd = new Random();

    public static CoinSide1 flip() {
        return rnd.nextBoolean() ? HEADS : TAILS;
    }

    public static void main(String[] args) {
        System.out.println(flip());
    }
}
