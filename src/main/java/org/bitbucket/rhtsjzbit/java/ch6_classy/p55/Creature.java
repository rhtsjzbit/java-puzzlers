package org.bitbucket.rhtsjzbit.java.ch6_classy.p55;

import java.util.concurrent.atomic.AtomicLong;

public class Creature {
    private static AtomicLong numCreated;

    public Creature() {
        numCreated.incrementAndGet();
    }

    public static long numCreated() {
        return numCreated.get();
    }
}
