package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p40;

public class Reluctant {

    private Reluctant internalInstance = new Reluctant();

    public Reluctant() throws Exception {
        throw new Exception("I'am not coming out");
    }

    public static void main(String[] args) {
        try {
            Reluctant b = new Reluctant();
            System.out.println("Surprise!");
        } catch (Exception e) {
            System.out.println("I told you so");
        }
    }
}
