package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p41;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FieldAndStream {

    public static void main(String[] args) {
        copy("/tmp/src.txt", "/tmp/dst.txt");
    }

    static void copy(String src, String dst) {

        InputStream in = null;
        OutputStream out = null;
        try {
            File inFile = new File(src);
            if (!inFile.exists()) {
                inFile.createNewFile();
            }
            File dstFile = new File(dst);
            if (!dstFile.exists()) {
                dstFile.createNewFile();
            }
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            byte[] buf = new byte[1024];
            int n;
            while ((n = in.read(buf)) >= 0) {
                out.write(buf, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeIgnoringException(in);
            closeIgnoringException(out);
        }
    }

    private static void closeIgnoringException(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
