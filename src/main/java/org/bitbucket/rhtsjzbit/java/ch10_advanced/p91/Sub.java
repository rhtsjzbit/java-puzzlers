package org.bitbucket.rhtsjzbit.java.ch10_advanced.p91;

final class Sub extends Super {
    private int id;

    public Sub(int id) {
        this.id = id;
        set.add(this); // Establish invariant
    }

    public void checkInvariant() {
        if (!set.contains(this)) {
            throw new AssertionError("invariant violated");
        }
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Sub) && (id == ((Sub) obj).id);
    }
}
