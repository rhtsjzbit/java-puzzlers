package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p45;

public class Workout {

    public static void main(String[] args) {
        try {
            workHard(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("It's nap time.");
    }

    private static void workHard(int count) {
        try {
            workHard(++count);
        } finally {
            System.out.println(count);
        }
    }
}
