package org.bitbucket.rhtsjzbit.java.ch2_expressive.p09;

/**
 * In summary, compound assignment operators silently generate a cast. If the
 * type of the result of the computation is wider than that of the variable, the generated
 * cast is a dangerous narrowing cast.
 */

public class Tweedledum {
    public static void main(String[] args) {
        short x = 0;
        int i = 123456;

        x += i;
        System.out.println(x);
//        x = x + i;
        System.out.println(x);
    }
}
