package org.bitbucket.rhtsjzbit.java.ch3_character.p14;

/**
 * Java provides no special treatment for Unicode escapes within string literals. The compiler translates Uni-
 * code escapes into the characters they represent before it parses the program into
 * tokens, such as strings literals [JLS 3.2].
 */

public class EscapeRout {


    public static void main(String[] args) {
        // Do not use Unicode escapes to represent ASCII characters
        // \u0022 is the Unicode escape for double quote (")
        System.out.println("a\u0022.length() + \u0022b".length());
    }
}
