package org.bitbucket.rhtsjzbit.java.ch3_character.p18;

import java.io.UnsupportedEncodingException;

public class StringCheese {

    public static void main(String[] args) throws UnsupportedEncodingException {
        byte[] bytes = new byte[256];
        for (int i = 0; i < 256; i++) {
            bytes[i] = (byte) i;
        }
//        String str = new String(bytes);
        String str = new String(bytes, "iso-8859-1");
        for (int i = 0, n = str.length(); i < n; i++) {
            System.out.println((int) str.charAt(i) + " ");
        }
    }
}
