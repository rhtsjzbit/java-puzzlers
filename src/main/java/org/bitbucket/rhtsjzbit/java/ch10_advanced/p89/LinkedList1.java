package org.bitbucket.rhtsjzbit.java.ch10_advanced.p89;

public class LinkedList1<E> {
    //Change instance fields of a class only in its own instance methods.

    private Node<E> head = null;


    /**
     * Static nested classes do not have access to the type parameters of
     * enclosing classes
     *
     * @param <T>
     */
    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

    }

    public void add(E e) {
        head = new Node<>(e, head);
    }

    public void dump() {
        for (Node<E> n = head; n != null; n = n.next) {
            System.out.print(n.value + " ");
        }
    }

    public static void main(String[] args) {
        LinkedList1<String> list = new LinkedList1<>();
        list.add("world");
        list.add("Hello");
        list.dump();
    }
}
