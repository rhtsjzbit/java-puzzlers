package org.bitbucket.rhtsjzbit.java.ch6_classy.p50;

public class Type2 {
    public static void main(String[] args) {
        /**
         * the instanceof operator requires
         that if both operands are class types, one must be a subtype of the other
         [JLS 15.20.2, 15.16, 5.5]
         */
//        System.out.println(new Type2() instanceof String);
    }
}
