package org.bitbucket.rhtsjzbit.java.ch8_classier.p75;

import java.util.Random;

public enum CoinSide2 {
    HEADS, TAILS;

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    private static Random rnd = new Random();

    public static CoinSide2 flip() {
        return rnd.nextBoolean() ? HEADS : TAILS;
    }

    public static void main(String[] args) {
        System.out.println(flip());
    }
}
