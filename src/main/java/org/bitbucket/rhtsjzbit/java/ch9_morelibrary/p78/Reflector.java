package org.bitbucket.rhtsjzbit.java.ch9_morelibrary.p78;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * In summary, it is illegal to access a member of a nonpublic type in a different
 * package, even if the member is also declared public in a public type.
 * This is true whether the member is accessed normally or reflectively.
 */

public class Reflector {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        /*
         * You cannot legally access a member of a nonpublic type from
         another package
         */
        Set<String> s = new HashSet<>();
        s.add("foo");

        Iterator it = s.iterator();
        /*
         * When accessing a type reflectively, use a Class object that represents an
         accessible type
         */
//        Method m = it.getClass().getMethod("hasNext");
        Method m = Iterator.class.getMethod("hasNext");
        System.out.println(m.invoke(it));
    }
}
