package org.bitbucket.rhtsjzbit.java.ch10_advanced.p90;

public class Outer {
    public Outer() {
    }

    class Inner1 extends Outer {
        public Inner1() {
            super();
        }
    }

    class Inner2 extends Inner1 {
        public Inner2() {
            super();
        }
    }
}
