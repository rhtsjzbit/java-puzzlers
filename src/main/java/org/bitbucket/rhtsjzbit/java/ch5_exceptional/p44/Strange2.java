package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p44;

public class Strange2 {
    public static void main(String[] args) {
        Missing m;
        try {
            m = new Missing();
        } catch (NoClassDefFoundError e) {
            System.out.println("Got it");
        }
    }
}
