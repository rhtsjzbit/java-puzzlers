package org.bitbucket.rhtsjzbit.java.ch5_exceptional.p36;

public class Indecisive {
    public static void main(String[] args) {
        System.out.println(decision());
    }

    // 在一个 try-finally语句中,finally语句块总是在控制权离开try语句时执行[JLS14.20.2]

    static boolean decision() {
        try {
            return true;
        } finally {
            return false;
        }
    }
}
