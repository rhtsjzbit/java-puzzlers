package org.bitbucket.rhtsjzbit.java.ch10_advanced.p93;

public class Words {
    // compile-time constant expression [JLS 15.28]
    // null is not a compile-time constant expression

    private Words() {
    }

    public static final String FIRST = "physics";
    public static final String SECOND = "chemistry";
    public static final String THIRD = "biology";

    // 通过添加一个方法,这个方法只是返回它的参数
}
