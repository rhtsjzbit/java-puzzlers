package org.bitbucket.rhtsjzbit.java.ch8_classier.p67;


/**
 * avoid the reuse of class
 * names, especially Java platform class names
 */
public class StrungOut {
    public static void main(java.lang.String[] args) {
        String s = new String("hello world");
        System.out.println(s);
    }
}

class String {
    private final java.lang.String s;

    public String(java.lang.String s) {
        this.s = s;
    }

    @Override
    public java.lang.String toString() {
        return s;
    }
}


