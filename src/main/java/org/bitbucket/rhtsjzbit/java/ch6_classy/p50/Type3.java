package org.bitbucket.rhtsjzbit.java.ch6_classy.p50;

public class Type3 {
    public static void main(String[] args) {
        cast(new Type3());
    }

    private static void cast(Object o) {
        Type3 t3 = (Type3) o;
    }
}
