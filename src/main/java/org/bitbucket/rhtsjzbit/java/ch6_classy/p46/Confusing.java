package org.bitbucket.rhtsjzbit.java.ch6_classy.p46;

public class Confusing {

    // overloading 重载会引起困惑

    private Confusing(Object o) {
        System.out.println("Object");
    }

    private Confusing(double[] dArray) {
        System.out.println("double array");
    }

    public static void main(String[] args) {
        new Confusing(null);
    }
}
