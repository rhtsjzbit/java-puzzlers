package org.bitbucket.rhtsjzbit.java.ch6_classy.p51;

public class Point {
    private final int x, y;
    private final String name; // Cached at construction time

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.name = makeName();
    }

    protected String makeName() {
        return "[" + x + "," + y + "]";
    }

    public final String toString() {
        return name;
    }
}
