package org.bitbucket.rhtsjzbit.java.ch2_expressive.p10;

public class Tweedledee {
    public static void main(String[] args) {
        Object x = "Buy ";
        String i = "Effective Java!";
        x = x + i;
        System.out.println(x);
        x += i;
        System.out.println(x);
    }
}
