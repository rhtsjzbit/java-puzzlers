package org.bitbucket.rhtsjzbit.java.ch8_classier.p66;

public class PrivateMatter {
    public static void main(String[] args) {
//        System.out.println(new Derived().className);
        // Hiding
        System.out.println(((Base) new Derived()).className);

        // Overriding
        System.out.println(new Derived().getClassName());
    }
}
