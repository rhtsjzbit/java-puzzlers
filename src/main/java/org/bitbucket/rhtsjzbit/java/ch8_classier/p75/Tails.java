package org.bitbucket.rhtsjzbit.java.ch8_classier.p75;

class Tails extends CoinSide {
    private Tails() {
    }

    public static final Tails INSTANCE = new Tails();

    @Override
    public String toString() {
        return "tails";
    }
}
