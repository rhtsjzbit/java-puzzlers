package org.bitbucket.rhtsjzbit.java.ch10_advanced.p95;

import java.util.Arrays;
import java.util.Comparator;

public class BananaBread {
    public static void main(String[] args) {
        Integer[] array = {3, 1, 4, 1, 5, 9};
        Arrays.sort(array, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 < o2 ? -1 : (o2 > o1 ? 1 : 0);
            }
        });

        System.out.println(Arrays.toString(array));
    }
}
